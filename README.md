# IPO2

This is an experimental v2 of the IPO package for XCMS parameter optimization (http://bioconductor.org/packages/IPO/).

Associate Professor Carl Brunius <carl.brunius@chalmers.se>  
Food and Nutrition Sciences  
Department of Life Sciences  
Chalmers University of Technology  

## Major changes from original IPO package
- Changed from RSM to NLOPT
- Switched from XCMS2 to XCMS3
- Removed optimization for retCor

## Major issues
- The pilot PP Parameter Optimizer works for centWave using XCMS3 syntax
- "NLOPT_LN_NELDERMEAD" converges more rapidly into good parameter settings cmp to other algos 
- "NLOPT_GN_DIRECT_L" seems to generate higher IPO score in early tests, but requires more iterations -> more time
- Although IPOv2 works on XCMS3 syntax, the internal IPO scoring is calculated from XCMS2 achieved by `xset <- as(xchr, 'xcmsSet')`. This should probably be addressed by rewriting IPO2:::calcPPS to work on XCMS3/XCMS4
- It's really easy to adapt the Optimizer to suit other methods & syntaxes (XCMS4) 

## Installation
Install development version using:

```R
if (!require("remotes", quietly = TRUE))
    install.packages("remotes")
remotes::install_gitlab("CarlBrunius/IPO2") 
```

## Usage

```
rm(list = ls())
 
library(IPO2)
library(BiocParallel)

files <- choose.files() # Select some files for IPO optimization of PP parameters 
 
# This workflow uses XCMS3 syntax (compatible also with XCMS4)

# Set starting parameters
cwParam <- CentWaveParam(noise = 400, prefilter = c(3, 1500), snthresh = 10, peakwidth = c(7, 55), ppm = 20, mzdiff = -0.001)
# Select which parameters should be optimized
optimVars <- c("min_peakwidth", "max_peakwidth", "mzdiff", "ppm" )

# set upper & lower bounds for parameters 
upper <- c(10, Inf, Inf, Inf)
lower <- c(2, 30, -Inf, 5)

# # List GLOBAL and LOCAL nlopt algos NOT relying on derivative
# nloptr::nloptr.get.default.options()$possible_values[1] %>% strsplit(.,', ') %>% unlist() %>% grep('_GN_', ., fixed = T, value = T)
# nloptr::nloptr.get.default.options()$possible_values[1] %>% strsplit(.,', ') %>% unlist() %>% grep('_LN_', ., fixed = T, value = T)
 
algorithm <- "NLOPT_LN_NELDERMEAD"
opt <- optimXCMS(files = files, cwParam = cwParam, optimVars = optimVars, upper = upper, lower = lower, algorithm = algorithm, verbose = TRUE)
 
opt
```

## Version history
version | date | comment
:------ | :--- | :------
0.2 | 2024-04-03 | Changed from symmetric bounds around initial guess to separate upper and lower bounds 
0.1 | *date* | Hello world - 1st working version with centwave in XCMS3 syntax
